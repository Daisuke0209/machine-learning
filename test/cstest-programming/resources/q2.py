import re

def file_to_string(filename):
    # TODO ファイルから内容を文字列として読みだす
    file = open(filename, 'r')  #読み込みモードでオープン
    string = file.read()      #readですべて読み込む
    return string
    
def string_to_words(str):
    # TODO 文字列を単語のリストに分割する
    a = str.split()
    a.sort()
    return a
    
    
def words_to_count(word_list):
    # TODO 単語リストから、各単語とその出現回数を対応付ける辞書を作る
    kari = []
    kari_num = []
    for i in range(len(word_list)):
        if word_list[i] not in kari:
            kari.append(word_list[i])
    
    for i in range(len(kari)):
        n = 0
        for j in range(len(word_list)):
            if kari[i] == word_list[j]:
                n = n + 1
        kari_num.append(n)
    
    dic = dict(zip(kari,kari_num))
    
    return dic
        
    
    
def print_words(word_dict):
    # TODO 辞書の中の単語とその出現回数を出現回数の多い順に表示する
    for k, v in sorted(word_dict.items(), key=lambda x: -x[1]):
        print(str(k) + ": " + str(v))
    
    
# 試験/評価用につき以下編集不可
def question2(filename):
    print_words(words_to_count(string_to_words(file_to_string(filename))))