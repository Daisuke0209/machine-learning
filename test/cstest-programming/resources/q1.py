import math

def normalize_image(image, average, stdev):

    a = [[0,0,0,0],[0,0,0,0],[0,0,0,0],[0,0,0,0]]
    for i in range(4):
        for j in range(4):
            a[i][j] = (image[i][j]-average)/stdev*16+64
            a[i][j] = round(a[i][j])
    return a

def calculate_average(image):
    sum = 0
    for i in range(len(image)):
        for j in range(len(image[0][:])):
            sum = sum + image[i][j]
    sum = sum/(len(image)*len(image[0][:]))
    return sum

def calculate_stdev(image):
    s2 = 0
    for i in range(len(image)):
        for j in range(len(image[0][:])):
            s2 = s2 + (calculate_average(image)-image[i][j])**2
            s2 = s2/(len(image)*len(image[0][:]))
            s2 = math.sqrt(s2)
    
    return s2

# 試験/評価用につき以下編集不可
def question1(image):
    average = calculate_average(image)
    stdev = calculate_stdev(image)
    print(normalize_image(image, average, stdev))
